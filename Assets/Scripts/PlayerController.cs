using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.Audio;

public class PlayerController : MonoBehaviour
{
    //Music
    [Header("Music")]
    [SerializeField] private AudioSource Engine;
    [SerializeField] private AudioSource Nitro;
    [SerializeField] private List<AudioClip> Sounds = new List<AudioClip>();
    [HideInInspector] public bool FirstEngineStart = false;

    //Player constant
    [Header("Basic Plyer Value")]
    [SerializeField] private float MoveSpeed = 10;
    [SerializeField] private float Boost = 1.5f;
    [SerializeField] private float RotationSpeed = 5f;
    [SerializeField] private Vector3 StartPosition = new Vector3(0, 0, 0);
    [SerializeField] private float NitroCount = 10;
    private float DopNitroCount;
    private bool ByBoost = false;

    //Ui
    [Header("UI")]
    [SerializeField] private Image NitroImage;

    //Particles
    [Header("Particles")]
    [SerializeField] private GameObject StandartParticle;
    [SerializeField] private GameObject NitroParticle;

    private Vector3 LastPosition;
    private Music AudioSystem;

    // InputSystem
    private InputManager PlayerInputSystem;
    private CharacterController PlayerContoller;

    private float gravityValue = -9.81f;
    private Vector3 playerVelocity;
    //private bool groundedPlayer;

    ////Player RigidBody
    //private Rigidbody PlayerRigidbody;

    //private Transform mainCam;

    private void Awake()
    {
        DopNitroCount = NitroCount;
        PlayerInputSystem = new InputManager();
        //PlayerRigidbody = GetComponent<Rigidbody>();
        PlayerContoller = GetComponentInChildren<CharacterController>();

        //subscribe
        PlayerInputSystem.Player.ResetPosition.performed += context => ResetPosition();
        PlayerInputSystem.Player.Boost.performed += context => BoostPlayer();
        PlayerInputSystem.Player.Boost.canceled += context => BoostPlayer();

        //mainCam = Camera.main.transform;
    }

    private void Start()
    {
        AudioSystem = Music.main;
    }

    private void OnEnable()
    {
        PlayerInputSystem.Enable();
    }

    private void OnDisable()
    {
        PlayerInputSystem.Disable();
    }

    private void Update()
    {
        LastPosition = transform.position;

        // Move with boost or no
        float boost = 1;
        if (ByBoost && NitroCount > 0)
        {
            boost = Boost;
        }

        NitroUpdate();

        if (ByBoost && Nitro.isPlaying is false)
            AudioSystem.PlayAudio(Nitro, Sounds[2]);

        Vector2 moveDirection = PlayerInputSystem.Player.Move.ReadValue<Vector2>();
        Vector3 move = transform.forward * moveDirection.y + transform.right * moveDirection.x;
        move.y = 0;
        PlayerContoller.Move(move * Time.deltaTime * MoveSpeed * boost);

        PlayKartMusic();

        Vector2 rotationDirection = PlayerInputSystem.Player.Rotation.ReadValue<Vector2>();
        Rotation(rotationDirection);

        //Gravitation
        Gravitation();
    }

    public void PlayKartMusic()
    {
        if ((Engine.isPlaying is false || LastPosition != transform.position) && FirstEngineStart is false)
        {
            AudioSystem.PlayAudio(Engine, Sounds[0]);

        }

        if (Engine.clip.name != Sounds[4].name)
        {
            if (LastPosition != transform.position && Engine.clip.name != Sounds[3].name)
            {
                AudioSystem.PlayAudio(Engine, Sounds[3]);
                FirstEngineStart = true;
            }
            else if (LastPosition == transform.position && Engine.clip.name != Sounds[0].name)
            {
                AudioSystem.PlayAudio(Engine, Sounds[0]);
            }
        }
    }

    public void Gravitation()
    {
        playerVelocity.y += gravityValue * Time.deltaTime;
        PlayerContoller.Move(playerVelocity * Time.deltaTime);
    }

    private void ResetPosition()
    {
        PlayerContoller.Move(new Vector3(0, 1000, 0));
        PlayerContoller.Move(StartPosition - transform.position);
        transform.localRotation = new Quaternion(0, 0, 0, 0);
        FirstEngineStart = false;
        LastPosition = transform.position;
        AudioSystem.PlayAudio(Engine, Sounds[4], false);
        Game.main.RaceTimer = 0;
        Game.main.UpdateRaceTimerText();
    }

    public void NitroUpdate()
    {
        if (ByBoost && NitroCount > 0)
            NitroCount -= Time.deltaTime;
        else if (NitroCount < DopNitroCount && ByBoost is false)
            NitroCount += Time.deltaTime;

        NitroImage.fillAmount = NitroCount / DopNitroCount;
    }

    private void BoostPlayer()
    {
        ByBoost = !ByBoost;
        if(ByBoost)
        {
            AudioSystem.PlayAudio(Nitro, Sounds[1], false);
            ChangeParticleColor();
        }
        else
        {
            AudioSystem.PlayAudio(Nitro, null);
            ChangeParticleColor();
        }
    }

    public void ChangeParticleColor()
    {
        StandartParticle.SetActive(!StandartParticle.activeSelf);
        NitroParticle.SetActive(!NitroParticle.activeSelf);
    }

    private void Rotation(Vector2 rotation)
    {
        transform.Rotate(Vector3.up * rotation.x * RotationSpeed);
    }

    //public void RotatePlayer(Vector2 moveDirection)
    //{
    //    if (moveDirection != Vector2.zero)
    //    {
    //        var child = transform.GetChild(0).transform;
    //        Quaternion rotation = Quaternion.Euler(new Vector3(child.localEulerAngles.x, mainCam.localEulerAngles.y, child.localEulerAngles.z));
    //        child.rotation = Quaternion.Lerp(child.rotation, rotation, Time.deltaTime * RotationSpeed);
    //    }
    //}

    //private void Move(Vector2 direction, bool byBoost = false)
    //{
    //    float boost = 1;
    //    if (byBoost)
    //        boost = Boost;
    //    PlayerRigidbody.velocity = new Vector3(direction.x * MoveSpeed * Time.deltaTime * boost, 0, 
    //        direction.y * MoveSpeed * Time.deltaTime * boost);
    //}
}
