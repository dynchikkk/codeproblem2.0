using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var MasterScript = Game.main;

        if (MasterScript.BestTime > MasterScript.RaceTimer)
        {
            MasterScript.BestTime = MasterScript.RaceTimer;
            MasterScript.UpdateBestTimeText();
        }
        
        MasterScript.RaceTimer = 0;
        MasterScript.UpdateRaceTimerText();
    }
}
