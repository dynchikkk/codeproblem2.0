using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public static Music main;

    [Header("Radio")]
    [SerializeField] private List<AudioClip> RadioMusic;
    [SerializeField] private AudioSource RadioSource;
    private int CurrentRadioTrack = 0;

    private void Awake()
    {
        main = this;
    }

    public void PlayAudio(AudioSource gramophone, AudioClip sound, bool loop = true)
    {
        gramophone.clip = sound;
        gramophone.loop = loop;
        gramophone.Play();
    }

    private void Update()
    {
        if (RadioSource.isPlaying is false)
            ChangeStation();
    }

    public void ChangeStation()
    {
        if (CurrentRadioTrack >= RadioMusic.Count)
            CurrentRadioTrack = 0;
        PlayAudio(RadioSource, RadioMusic[CurrentRadioTrack]);
        CurrentRadioTrack += 1;
    }
}
