using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Game : MonoBehaviour
{
    public static Game main;
    public GameObject Player;

    //timer
    [Header("Time")]
    public float RaceTimer = 0;
    public Text RaceTimerText;
    public float BestTime = 100000;
    public Text BestTimeText;

    private void Awake()
    {
        main = this;
        UpdateRaceTimerText();
    }

    private void Update()
    {
        //timer
        if (Player.GetComponent<PlayerController>().FirstEngineStart)
        {
            RaceTimer += Time.deltaTime;
            UpdateRaceTimerText();
        }
    }

    public void UpdateRaceTimerText()
    {
        TimeSpan ts = TimeSpan.FromSeconds(RaceTimer);
        RaceTimerText.text = $"{Convert.ToInt32(ts.Minutes)}.{Convert.ToInt32(ts.Seconds)}.{Convert.ToInt32(ts.Milliseconds)}";
    }

    public void UpdateBestTimeText()
    {
        TimeSpan ts = TimeSpan.FromSeconds(BestTime);
        BestTimeText.text = $"Best time\n{Convert.ToInt32(ts.Minutes)}.{Convert.ToInt32(ts.Seconds)}.{Convert.ToInt32(ts.Milliseconds)}";
    }
}
